<?php

    include 'apitomic.crypto.php'; 
    include 'apitomic.utils.php'; 
    include 'apitomic.php';
    
    $utils = new Utils();
    $config = $utils->readConfig();

    try {            
    
        $config = processConfig($config);     
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
            // array holding allowed Origin domains

            $allowedOrigins = $config->allowedOrigins;

            if ($allowedOrigins && is_array($allowedOrigins)){

                // you want to allow, and if so:
            
                if (in_array($_SERVER['HTTP_ORIGIN'], $allowedOrigins)) {
                    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                    header('Access-Control-Allow-Credentials: true');
                    header('Access-Control-Max-Age: 86400');    // cache for 1 day
                } else {                    
                    header("HTTP/1.1 404 Not Found");
                    header("Content-Type: text/plain");
                    die("NOT FOUND");
                }
            

            } else {            
                header("Access-Control-Allow-Origin: *");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                // may also be using PUT, PATCH, HEAD etc
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        } 

        $namespacePos = strpos ( $_SERVER['REQUEST_URI'] , $config->namespace );

        if ($namespacePos){
            $namespaceURL = substr ($_SERVER['REQUEST_URI']  , 0, ($namespacePos + strlen ($config->namespace)));
            $relativeURL = substr ($_SERVER['REQUEST_URI']  , ($namespacePos + strlen ($config->namespace)));
            $url = $utils->mappingReplace($relativeURL, $config->mask);
            
            if ($url){
                $url = $namespaceURL.$url;
                $array = explode("/", $url);
            } else {
                print_json(404, "Not Found", null); 
                die();
            }
            
        } else {
            print_json(404, "Not Found", null); 
            die();
        }

        // Obtener el cuerpo de la solicitud HTTP
        // En nuestro caso, el cuerpo solo sera enviado en peticiones de tipo POST y PUT, en el cual enviaremos el objeto JSON a registrar o modificar
        $bodyRequest = file_get_contents("php://input");
        
        //elimina el ultimo  nodo vacio.
        foreach ($array as $key => $value) {
            if(empty($value)) {
                unset($array[$key]);
            }
        }

        $key = array_search($config->namespace, $array);
        $newArray = array_slice($array, $key-1);

        //var_dump($newArray);
        $showInfo = false;
        if($array[count($array) - 4] == $config->namespace) {
            $type = $array[count($array)];
            $filter = $array[count($array) - 1];
            $param = $array[count($array) - 2];
            $key = $array[count($array) - 3];
        } else if($array[count($array) - 3] == $config->namespace) {
            $filter = $array[count($array)];
            $param = $array[count($array) - 1];
            $key = $array[count($array) - 2];
        } else if($array[count($array) - 2] == $config->namespace) {
            $param = $array[count($array)];
            $key = $array[count($array) - 1];
        } else if($array[count($array)] == $config->namespace) {
            $showInfo = $config->showReactor;
        } else {
            if (count($newArray) == 2){            
                $key = $array[count($array)];
            }
        }

        // Variable que guarda la instancia de la clase apitomic.
        $apitomicParticle = new APItomic($config);    

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'OPTIONS': 
                print_json(200, "OK", null);
            break;
            case 'GET': 

            if($showInfo){

                $showInfo = false;
                header("Location: ./reactor/");  
                
            } else {
                $isGranted = true; 
                $jwt = false; 
                
                if($key == "relativity"){

                    $data = $apitomicParticle->getRelativityMask();

                    if($data) {    
                        if(count($data) === 0 || $data->count === 0) {
                            print_json(404, "Not Found", null);                        
                        } else {
                            print_json(200, "OK", $data);
                        }

                    } else {
                        print_json(404, "Not Found", null);
                    }
                } else {
                    
                    if($config->useJWT){
                        $isGranted = false;
                        $token = getBearerToken();
                        if(!empty($token)){
                            $validationObj = $apitomicParticle->validateToken($token);
                            $isGranted = ($validationObj->message == "Access granted.") ? true : false;
                            if ($isGranted && $validationObj->jwt){
                                $jwt = $validationObj->jwt;
                            }
                        }
                    }

                    if($isGranted){

                        $data = getReaction($key, $param, $apitomicParticle, $filter);

                        if($key == "relativity"){
                            $data = $apitomicParticle->getRelativityMask();
                        }

                        if($data) {    
                            if(count($data) === 0 || $data->count === 0) {
                                print_json(404, "Not Found", null);                        
                            } else {
                                print_json(200, "OK", $data);
                            }

                        } else {
                            print_json(404, "Not Found", null);
                        }

                    } else{
                        print_json(404, "Not Found", false);
                    }
                }
            }
                
            break;

            case 'POST':          
                
            $isGranted = true;  
            $isAccesDenied = true;              
            $obj = json_decode($bodyRequest);
            
            if($config->useJWT){
                $isGranted = false;
                $token = getBearerToken();
                if(!empty($token)){
                    $validationObj = $apitomicParticle->validateToken($token);
                    $isGranted = ($validationObj->message == "Access granted.") ? true : false;
                    if ($isGranted){

                        if($key == "updateuser"){             
                            $updateResponse = $apitomicParticle->updateUser($token, $obj);
                            if (is_object($updateResponse) && $updateResponse->message == "Access denied."){
                                print_json(404, "Not Found", false);
                                $isGranted = false;
                                $isAccesDenied = false;
                            } else {
                                if($updateResponse){
                                    print_json(200, "Saved", $updateResponse);                        
                                } else {                                
                                    print_json(404, "Not Found", null);  
                                }
                                $isGranted = false;
                                $isAccesDenied = false;                  
                            }
                        } else if($key == "invalidatetoken") {
                            $response = $apitomicParticle->invalidateToken($token);
                            if (is_object($response)){
                                print_json(200, "Token Removed", $response);                  
                            } else {
                                print_json(404, "Not Found", null);                 
                            }
                            $isGranted = false;
                            $isAccesDenied = false;  
        
                        } else if($key == "auth") {                    
                            print_json(200, "Already logged in.", null);
                            $isGranted = false;
                            $isAccesDenied = false;                     
                        } else {
                            $apitomicParticle->secureObj($obj);
                            $json = json_encode($obj);
                            if(strpos($json, 'Unable to update.') !== false){
                                print_json(404, "Not Found", null);
                                $isGranted = false;
                                $isAccesDenied = false;
                            }
                        }
                    }
                } else {
                    if($key == "auth") {
                        $data = $apitomicParticle->auth($obj);
                        if($data){
                            print_json(200, "Successful login.", $data);
                        } else {                        
                            print_json(404, "Not Found", null);
                        }

                        $isAccesDenied = false;

                    } else if($key == "registeruser") {
                        $regUserResponse = $apitomicParticle->registerUser($obj);
                        if ($regUserResponse === true) {
                            print_json(200, "User was created.", null);
                        } else {
                            if($regUserResponse === false){
                                print_json(404, "Not Found", null);                          
                            } else {
                                print_json(200, "User was created but ".$regUserResponse, null);
                            }               
                        }                        

                        $isAccesDenied = false;
                    } else if($key == "forgotpwdreq"){                        
                        $data = $apitomicParticle->forgotpwdreq($obj);
                        print_json(200, "An email was sent to your inbox with the instructions about how to reset your password.", null);
                        $isAccesDenied = false;
                    } else if($key == "resetpwd"){
                        
                        $data = $apitomicParticle->resetpwd($obj);
                        if($data === true){
                            print_json(200, "Password changed successfully.", null);
                        } else {   
                            if ($data === false){
                                print_json(404, "Not Found", null);
                            }                     
                        }

                        $isAccesDenied = false;
                    }
                }
            }

            if($isGranted){   

                if(isset($key)) {

                    if($key == "apitomize") {
                        $currentUser = $apitomicParticle->getCurrentUser();
                        if($config->useJWT && $utils->hasReservedClass($obj) && !$currentUser->coreUser){ 
                            $data = false;
                        } else {           
                            $data = $apitomicParticle->apitomize($obj);
                        }
        
                        if($data) {
                            if ($data->class === "APITomicFormulaResult"){                                
                                print_json(200, "Ok", $data);
                            } else {                                
                                print_json(200, "Saved", $data);
                            }
                        } else {                        
                            print_json(404, "Not Found", null);
                        }
                    } else if($key == "decay") {                 
                        $toRemove = getReaction($param, $filter, $apitomicParticle, $type, true);
        
                        if ($toRemove != false){
                                            
                            if(isset($toRemove->id))
                                $decay = $apitomicParticle->apitomicDecay($toRemove);
                            else if (isset($toRemove->members))
                                $decay = $apitomicParticle->apitomicDecay($toRemove->members);
                            
                            if($decay) {
                                print_json(200, "Removed.", $toRemove);
                            } else {
                                print_json(404, "Not Found", null);
                            }
                            
                        } else {
                            print_json(404, "Not Found", null);
                        }
                    } else {
                        print_json(404, "Not Found", null);
                    }

                } else {
                    print_json(404, "Not Found", null);
                }           

            } else {
                if ($isAccesDenied)
                    print_json(404, "Not Found", false);
            }
                
            break;        
    
            default:
                print_json(404, "Not Found", null);
            break;
        }
    } catch (Exception $e) {
        if (strtoupper($config->env) == "DEV"){
            $msj = "Exception: ".$e->getMessage()." file: ".$e->getFile()." line: ".$e->getLine();
            print_json(400, $msj, null);
        } else {
            print_json(404, "Not Found", null);
        }
    }
    catch (Throwable $e) {
        
        if (strtoupper($config->env) == "DEV"){
            $msj = "Exception: ".$e->getMessage()." file: ".$e->getFile()." line: ".$e->getLine();
            print_json(400, $msj, null);
        } else {
            print_json(404, "Not Found", null);
        }
    }

    // ---------------------- Funciones controladoras ------------------------------- //
    //GetReaction
    function getReaction($key, $param, $apitomicParticle, $filter=NULL, $decay=false){
        
        $reaction = $apitomicParticle->getReaction($key, $param, $filter, $decay);
            
        if ($reaction != false){
            if(isset($param) && is_object($reaction)) {
                if ($reaction->id){
                    foreach($reaction as $k => $val) {  
                        if ($k == $param){                      
                            $data = new stdClass();    
                            $data->value=$val;   
                            break;
                        }      
                    }
                } else {                
                    $data = $reaction;
                }
            } else if (!isset($param)){                
                $data = $reaction;
            }

            if($config->useJWT && $jwt){
                $data->jwt = $jwt;
            }

            return $data;
        }

        return false;
    }

    function processConfig($config){

        try{
            
            $crypto = new APITomicCrypto();

            $apitomicParticle = new APItomic($config);

            if ($apitomicParticle->apitomicDB->connection){

                $data = getReaction("class", "APITomic", $apitomicParticle);

                if (count($data->members)>0){

                    foreach($data->members as &$configSet) {
                        if (isset($configSet->settings)){
                            $plusConfig = $crypto->decrypt($configSet->settings);     
                            $plusConfigObj = json_decode($plusConfig);
                            $config = (object) array_merge((array) $config, (array) $plusConfigObj);                    
                        }
                    }
                } 

                return $config;

            } else {

                $error = 'Error: APITomic can not be instantiated.';
                throw new Exception($error);

            }

        } catch (Exception $e)  {
            throw $e;
        }
    }
   
    // Esta funcion imprime las respuesta en estilo JSON y establece los estatus de la cebeceras HTTP
    function print_json($status, $mensaje, $data) {
        header("HTTP/1.1 $status $mensaje");
        header("Content-Type: application/json; charset=UTF-8");

        $response['statusCode'] = $status;
        $response['statusMessage'] = $mensaje;
        $response['data'] = $data;

        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    //TOKEN

    /** 
     * Get header Authorization
     * */
    function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }
    /**
    * get access token from header
    * */
    function getBearerToken() {
        $headers = getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {

            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
?>