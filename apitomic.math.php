<?php 

class APITomicFormula
{
    var $apitomicParticle;
    var $utils;

    public function __construct(APITomic $apitomicParticle){
        // constructor code here it will be called once only
        $this->apitomicParticle = $apitomicParticle;
    }
        
    public function getResult($mathObj){
        // Get the math result of MathObj. 

        $i = 0;
        $map = [];
        $reactionArray = [];
        $prop = NULL;

        //Analizar si la operacion empieza con ["="/"?"] y remover ese caracter.
        if ($mathObj->operation){
            $opType = substr($mathObj->operation,0 , 1); 
            $operation = str_replace($opType, "", $mathObj->operation);
        } 

        foreach ($mathObj->args as $arg) {

            if (is_object($arg)) {

                if ($arg->_prop){

                    if ($arg->_id){

                        $argObj = $this->apitomicParticle->getReaction($arg->_id);                        
                        $mathObj->args[$i] = eval('return $argObj->'.$arg->_prop.';');
                        $arg = $mathObj->args[$i];
    
                    } else if ($arg->_class){

                        if ($prop===NULL || $opType == "="){
                            $reaction = $this->apitomicParticle->getReaction("class", $arg->_class);
                            $reactionArray = $reaction->members;

                            $prop = $arg->_prop;
                            
                            if (count($reactionArray) > 0){
                                unset($mathObj->args[$i]);
                                foreach ($reactionArray as $obj) {
                                    $val = eval('return $obj->'.$prop.';');
                                    if (!is_null($val))
                                        array_push($mathObj->args, $val);
                                }
                            }
                        }

                    } else {

                        if ($prop===NULL || $opType == "="){
                            $reaction = $this->apitomicParticle->getReaction($arg->_prop, "*");
                            $reactionArray = $reaction->members;

                            $prop = $arg->_prop;
                            
                            if (count($reactionArray) > 0){
                                unset($mathObj->args[$i]);
                                foreach ($reactionArray as $obj) {
                                    $val = eval('return $obj->'.$prop.';');
                                    if (!is_null($val))
                                        array_push($mathObj->args, $val);
                                }
                            } else {
                        
                                return false;

                            }
                        }
                    }
                    
                } else {
                    if ($arg->_class){

                        if ($prop===NULL || $opType == "="){
                            $reaction = $this->apitomicParticle->getReaction("class", $arg->_class);
                            $reactionArray = $reaction->members;
                            
                            if (count($reactionArray) > 0){
                                unset($mathObj->args[$i]);
                                foreach ($reactionArray as $obj) {
                                    array_push($mathObj->args, $obj);
                                }
                            }
                        }

                    } else {
                        return false;
                    }
                }

            }       

            if (is_string($arg)){  
                $arg = "'".$arg."'"; //extraer el valor de la reaccion especificada en el string $arg. 
            }

            $map += [strval($i) => $arg];

            $i++;
        }    
           
        if ($opType == "?"){
            if (count($reactionArray) > 0){
                //Filtrar el arreglo segun la operacion logica.
                $result = $this->logicFilter($reactionArray, $prop, $operation, $map);
            } else {
                if (count($map) > 0)
                    $operation = $this->apitomicParticle->utils->replace_tags($operation, $map);
                
                $result = eval("return ".$operation.";");
            }

            if(!is_array($result))
                $result = $result ? 'true' : 'false';

        } else { 

            $inputArray = $mathObj->args;

            if ($mathObj->filter){

                if($prop){

                    $inputArray = $this->logicFilter($inputArray, $prop, $mathObj->filter, $map);

                } else {                    
                    return false;
                }

            }

            if($inputArray) {
            
                $bulkOp =  strtoupper(substr($operation,0 , 3));            
                switch($bulkOp)
                {
                    case 'SUM':

                        $result = array_sum($inputArray);

                    break;
                    case 'AVG':

                        $result = array_sum($inputArray)/count($inputArray);

                    break;
                    case 'COU':

                        $result = count($inputArray);

                    break;
                    case 'MAX':
                        $result = max($inputArray);

                    break;
                    case 'MIN':

                        $result = min($inputArray);

                    break;
                    case 'DIS':

                        $result = array_values(array_unique($inputArray));

                    break;
                    default:
                        $result = true;
                        foreach ($map as $element) {
                            if (is_array($element)) {
                            // array
                                $result = false;
                                break;
                            } else if (is_object($element)) {
                            // object
                                $result = false;
                                break;
                            } else if (is_resource($element)) {
                            // resource
                                $result = false;
                                break;
                            }
                        }

                        if($result){
                            $operation = $this->apitomicParticle->utils->replace_tags($operation, $map);
                            $result = eval("return ".$operation.";");
                        } else {
                            throw new Exception('Invalid (map) input parameter for utils.replace_tags(operation, map)');
                        }
                    break;
                }
            }
        }
        
        if ($result){

            if ($result === 'true' || $result === 'false')
                $result = $result === 'true'? true: false;

            $newObj = new stdClass();
            $newObj->class = "APITomicFormulaResult";
            if (is_array($result))
                $newObj->count = count($result);
            $newObj->result = $result;

            return $newObj;
        }

        return false;
    }
    
    private function logicFilter($args, $prop, $operation, $map){

        $filteredArray = [];
        
        $operation = str_replace("{{0}}", "", $operation);
        $operation = $this->apitomicParticle->utils->replace_tags($operation, $map);

        foreach ($args as $arg) {
            $evaluation = false;

            $val = eval('return $arg->'.$prop.';');

            if($val){
                $evaluation = "return ". strval($val).$operation.";";
                if(is_string($val))
                    $evaluation = "return '". $val."'".$operation.";";
            } else if(!is_object($arg) && !is_null($arg)){      
                $evaluation = "return ". strval($arg).$operation.";";  
                if(is_string($arg))
                    $evaluation = "return '". $arg."'".$operation.";";
            }

            

            if(eval($evaluation)){            
                array_push($filteredArray, $arg);    
            }
        } 


        if(count($filteredArray)>0){

            return $filteredArray;

        }

        return false;
    }
}

?>