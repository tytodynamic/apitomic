<?php
class APItomicDB
{
    var $db_host;
    var $username;
    var $pwd;
    var $database;
    var $tablename;
    var $connection;

    static $_instance;

    private function __construct($host,$uname,$pwd,$database,$tablename)
    {
        $this->db_host  = $host;
        $this->username = $uname;
        $this->pwd  = $pwd;
        $this->database  = $database;
        $this->tablename = $tablename;       
        
        return $this->CreateTable();
    }

    public static function init($host,$uname,$pwd,$database,$tablename){
        if(self::$_instance == null){
            self::$_instance = new self($host,$uname,$pwd,$database,$tablename);
        }
        
        return self::$_instance;            
    }
    
    private function DBLogin()
    {

        $this->connection = mysqli_connect($this->db_host,$this->username,$this->pwd);

        if(!$this->connection)
        {   
            //echo "Database Login failed! Please make sure that the DB login credentials provided are correct";
            return false;
        }
        if(!mysqli_select_db($this->connection, $this->database))
        {
            //echo 'Failed to select database: '.$this->database.' Please make sure that the database name provided is correct';
            return false;
        }
        if(!mysqli_query($this->connection, "SET NAMES 'UTF8'"))
        {
            //echo 'Error setting utf8 encoding';
            return false;
        }
        return true;
    } 
    
    private function DBLogout()
    {
        if($this->connection){
            return mysqli_close($this->connection);
        }
        return false;
    } 
    
    private function CreateTable()
    {
        if($this->DBLogin())
        {
            $qry = "CREATE TABLE IF NOT EXISTS $this->tablename (".
            "id VARCHAR(32),".
            "parent VARCHAR(32),".
            "class VARCHAR(32),".
            "jdoc TEXT,".
            "PRIMARY KEY(id),".
            "INDEX class_parent (class, parent)".
            ")";
            
            if(!mysqli_query($this->connection, $qry))
            {
                //echo "Error creating the table \nquery was\n $qry";
                return false;
            }

            $this->DBLogout();

            return true;
        }

        //echo "Database login failed!";
        return false;
    }
    
    public function objExists($id)
    {
        if($this->DBLogin())
        {
            $qry = "select id from $this->tablename where id='".$this->SanitizeForSQL($id)."'";
            $result = mysqli_query($this->connection, $qry);   
            if($result && mysqli_num_rows($result) > 0)
            {                
                $this->DBLogout();
                return true;
            }
            
            $this->DBLogout();
        } 

        return false;
    }

    public function GetAllParents()
    {       
        if($this->DBLogin())
        {     
            $qry = "SELECT * FROM ". $this->tablename.
            " WHERE parent IS NULL OR parent =''";

            $result = mysqli_query($this->connection, $qry);
            
            $parents = [];
            
            if (mysqli_num_rows($result) > 0) {
                // output data of each row
                while($row = mysqli_fetch_assoc($result)) {        
                    array_push($parents, $row);
                }
            } else {
                //echo "There is no parent objects in the DB";
            
                $this->DBLogout();
                return false;
            }
            
            $this->DBLogout();
            return $parents;
        }         

        //echo "Database login failed!";
        return false;
    }

    public function GetByProp($key, $value, $stringtified=true)
    {            
        if($this->DBLogin())
        {         
            if($value == "*")
                $where = " WHERE ".$this->SanitizeForSQL($key)."  IS NOT NULL AND ".$this->SanitizeForSQL($key)." != ''";
            else
                $where = " WHERE ".$this->SanitizeForSQL($key)." = '".$this->SanitizeForSQL($value)."'";
            if ($key != "class" && $key != "parent" ){
                if ($stringtified) {
                    $filter = '"'.$this->SanitizeForSQL($key).'":"'.$this->SanitizeForSQL($value).'"';
                } else {
                    if(is_bool($value)){
                        if($value)
                            $filter = '"'.$this->SanitizeForSQL($key).'":true';
                        else
                            $filter = '"'.$this->SanitizeForSQL($key).'":false';
                    } else {
                        if ($value == "*")                            
                            $filter = '"'.$this->SanitizeForSQL($key).'":';
                        else                        
                            $filter = '"'.$this->SanitizeForSQL($key).'":'.$this->SanitizeForSQL($value);
                    }
                }
                $where = " WHERE jdoc LIKE '%".$filter."%'";
            }

            $qry = "SELECT * FROM ". $this->tablename.$where;

            $result = mysqli_query($this->connection, $qry);
            
            $objects = [];
            if ($result) {
                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while($row = mysqli_fetch_assoc($result)) {        
                        array_push($objects, $row);
                    }
                } else {
                    //echo "There is no objects for class ". $this->SanitizeForSQL($class) ." in the DB";                
                    $this->DBLogout();
                    return false;
                }
            }
            
            $this->DBLogout();
            return $objects;
        }

        //echo "Database login failed!";
        return false;

    }

    public function GetByID($id)
    {   
                  
        if($this->DBLogin())
        {          
            $qry = "SELECT * FROM ". $this->tablename.
            " WHERE id='".$this->SanitizeForSQL($id)."'";

            $result = mysqli_query($this->connection, $qry);
            
            if(!$result)
            {
                //echo "There is no object with id: $id";
                $this->DBLogout();
                return false;
            } 
            
            $apitomic_rec = mysqli_fetch_assoc($result);

            $this->DBLogout();
            return $apitomic_rec;
        }

        //echo "Database login failed!";
        return false;
    }
    
    public function Add(&$row)
    {          
        if($this->DBLogin())
        {     
            $insert_query = 'INSERT INTO '.$this->tablename.'(
                    id,
                    parent,
                    class,
                    jdoc
                    )
                    VALUES
                    (
                    "' . $this->SanitizeForSQL($row->id) . '",
                    "' . $this->SanitizeForSQL($row->parent) . '",
                    "' . $this->SanitizeForSQL($row->class) . '",
                    "' . $this->SanitizeForSQL($row->jdoc) . '"
                    )';      
            if(!mysqli_query($this->connection, $insert_query))
            {
                //echo "Error inserting data to the table\nquery:$insert_query";
                $this->DBLogout();
                return false;
            }
            
            $this->DBLogout();        
            return true;
        }

        //echo "Database login failed!";
        return false;
    }
    
    public function Update($row)
    {            
        if($this->DBLogin())
        {    
            $qry = "Update $this->tablename Set ".
            "jdoc='".$this->SanitizeForSQL($row->jdoc)."' ".
            "Where  id='".$this->SanitizeForSQL($row->id)."'";
            
            if(!mysqli_query($this->connection, $qry))
            {
                //echo "Error updating the object -> \nquery:$qry";
                $this->DBLogout(); 
                return false;
            }     
            $this->DBLogout(); 
            return true;
        }

        //echo "Database login failed!";
        return false;
    }
    
    public function Delete($id)
    {            
        if($this->objExists($id)){     
            if($this->DBLogin())
            { 
                $qry = "Delete FROM $this->tablename Where  id='".$this->SanitizeForSQL($id)."'";
                
                if(!mysqli_query($this->connection, $qry))
                {
                    //echo "Error deleting the object \nquery:$qry";
                    $this->DBLogout(); 
                    return false;
                }     
                $this->DBLogout(); 
                return true;
            }
        }

        //echo "Database login failed!";
        return false;
    }
    
    public function ClearAll()
    {          
        if($this->DBLogin())
        {        
            $qry = "Delete FROM ".$this->tablename;
            
            if(!mysqli_query($this->connection, $qry))
            {
                //echo "Error updating the password \nquery:$qry";
                $this->DBLogout(); 
                return false;
            }     
            $this->DBLogout(); 
            return true;
        }

        //echo "Database login failed!";
        return false;
    }

    public function SanitizeForSQL($str)
    {
        if( function_exists( "mysqli_real_escape_string" ) )
        {
              $ret_str = mysqli_real_escape_string( $this->connection, $str );
        }
        else
        {
              $ret_str = addslashes( $str );
        }
        return $ret_str;
    }
}

class APItomicDataRow {
    public $id;
    public $parent;
    public $class;
    public $jdoc;

    function __construct ( $i, $c, $p=NULL ) {
        $this->id = $i;
        $this->parent = $p;
        $this->class = $c;
        $this->jdoc = $j;
    }
}

?>