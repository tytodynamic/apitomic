<?php

//PHPMailer
require 'vendor/phpmailer/phpmailer/class.phpmailer.php';

class Utils
{
    public function __construct(){
        // constructor code here it will be called once only
    }
    
    public function SendEmail($email, $subject, $body, $config)
    {
        try{
            $mailer = new PHPMailer(true);   
            
            $mailer->isSMTP();
    
            $mailer->Host = $config->smtp_host;
            $mailer->Port = $config->smtp_port;
            $mailer->SMTPAuth = $config->smtp_auth;
            $mailer->Username = $config->smtp_account;
            $mailer->Password = $config->smtp_password;        
            $mailer->SMTPSecure = $config->smtp_secure;
    
            $mailer->setFrom($config->smtp_account);        
            $mailer->addAddress($email);
    
            $mailer->isHTML(true);   
            
            $mailer->Subject = $subject;
            
            $mailer->Body = $body; 
            
            if(!$mailer->send())
            {
                return "Failed sending email: ". $mailer->ErrorInfo;
            }
            return true;

        } catch(Exception $e) {
            return "Failed sending email: {$e}";
        }
    }

    //////MISC////////////////////////////////////
    public function generateID ($apitomicDB){

        $result = md5(uniqid(mt_rand(), true).microtime(true));
        while ($apitomicDB->objExists($result)) {
            $result = md5(uniqid(mt_rand(), true).microtime(true));
        }

        return $result;

    }

    public function binarySearch($items, $value){

        usort($items, function ($a, $b)
        {
            return strcmp($a->id, $b->id);
        });

        $startIndex  = 0;
        $stopIndex   = count($items) - 1;
        $middle      = floor(($stopIndex + $startIndex)/2);
    
        while($items[$middle]->id != $value && $startIndex < $stopIndex){
    
            //adjust search area
            if ($value < $items[$middle]->id){
                $stopIndex = $middle - 1;
            } else if ($value > $items[$middle]->id){
                $startIndex = $middle + 1;
            }
    
            //recalculate middle
            $middle = floor(($stopIndex + $startIndex)/2);
        }
    
        //make sure it's the right value
        return ($items[$middle]->id != $value) ? false : $items[$middle];
    }
    
    public function isStringtified($string){
        $types = array('integer', 'float', 'boolean');
        foreach ($types as $type) {
            //if (strstr($string, $url)) { // mine version
            if (strpos($string, $type) !== FALSE) { // Yoshi version
                return false;
            }
        }
        return true;
    }
    /**
     * replace_tags replaces tags in the source string with data from the tags map.
     * The tags are identified by being wrapped in '{{' and '}}' i.e. '{{tag}}'.
     * If a tag value is not present in the tags map, it is replaced with an empty
     * string
     * @param string $string A string containing 1 or more tags wrapped in '{{}}'
     * @param array $tags A map of key-value pairs used to replace tags
     * @param force_lower if true, converts matching tags in string via strtolower()
     *        before checking the tags map.
     * @return string The resulting string with all matching tags replaced.
     */
    public function replace_tags($string, $tags, $force_lower = false)
    {
        return preg_replace_callback('/\\{\\{([^{}]+)\}\\}/',
            function($matches) use ($force_lower, $tags)
            {
                $key = $force_lower ? strtolower($matches[1]) : $matches[1];
                return array_key_exists($key, $tags) 
                    ? $tags[$key] 
                    : ''
                    ;
            }
            , $string);
    }

    public function generatePassword($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!?~@#-_+<>[]{}';
        $count = mb_strlen($chars);
    
        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
    
        return $result;
    }

    public function hasReservedClass($obj){ 
        $reservedClasses = array(
                                'APITomic', 
                                'User', 
                                'InvalidToken', 
                                'ForgotPasswordRequest', 
                                'APITomicFormula', 
                                'APITomicStoredFormula', 
                                'APITomicFormulaResult'
                            );
        // var_dump($obj);
        foreach ($obj as $k => $val){
            if (is_object($val)){ 
                // echo "class: ".$val->class."<br>";  
                if (in_array($val->class, $reservedClasses)){
                    return true;
                }
                array_walk_recursive($val, 'hasReservedClass');
            } else if (is_array($val)){
                foreach ($val as $v)
                {
                    if (is_object($v)){
                        // echo "class: ".$v->class."<br>";     
                        if (in_array($v->class, $reservedClasses)){
                            return true;        
                        }
                        array_walk_recursive($v, 'hasReservedClass');
                    }
                }
            } else if($k == "class"){
                if (in_array($val, $reservedClasses)){
                    return true;
                }
            }
        }       
        return false;
    }
    
    //mappingReplace
    public function mappingReplace($url, $map){
        // echo $url." \n";
        foreach($map as $key => $value) {
            // echo $key." => ". $value ." \n";
            if (isset($value) && trim($value)!=="" && $value!==$key){           
                
                //1. $key(reservedWord) should be searched into $url. If it's found should return false.
                if ( strpos( trim(strtolower($url)), trim(strtolower($key)) ) !== false ) {
                    // echo "url: ".$url." key: ".$key." strpos: ".strpos( $url, $key )." \n";
                    return false;
                } else {
                    //2. $value(mappedWord) should be searched into $url and replace back to $key(reservedWord).
                    $url = str_replace(trim(strtolower($value)), trim(strtolower($key)), trim(strtolower($url)));
                }

            }
        }

        return $url;
    }

    public function readConfig(){
        
        $defaultValues = '{
            "db_host": "localhost",    
            "db_port": "3306",
            "username": "root",
            "pwd": "root",
            "database": "tytodynamic",
            "tablename": "apitomic",
            "allowedOrigins": [
                "http://localhost:80",
                "http://localhost:81"
            ]
        }';
        $defaultConfigObj = json_decode($defaultValues);

        // Get the contents of the JSON file 
        $strJsonFileContents = file_get_contents("./apitomic_dev.json");
        $apitomicConfigObj = json_decode($strJsonFileContents);

        $configObj = (object) array_merge((array) $defaultConfigObj, (array) $apitomicConfigObj);
        
        return $configObj;
    }
}

?>