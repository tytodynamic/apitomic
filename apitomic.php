<?php

use Firebase\JWT\JWT;

// Load Composer's autoloader
require 'vendor/autoload.php';
include 'apitomic.math.php';
require_once("apitomic.db.php"); 

class APItomic {

    var $apitomicDB;
    var $childsArr;
    var $config;
    var $currentUser;
    var $formula;
    
    public $utils;

    public function __construct($config)
    {
        $this->apitomicDB = APItomicDB::init(
            ($config->db_port != "")?$config->db_host.':'.$config->db_port:$config->db_host, 
            $config->username, 
            $config->pwd, 
            $config->database, 
            $config->tablename
        );

        $this->config = $config;

        $this->formula = new APITomicFormula($this);
        $this->utils = new Utils();
        $this->crypto = new APITomicCrypto();
    }    

    public function getCurrentUser() {
        return $this->currentUser;
    }

    public function registerUser($jsonObj){

        if(strtoupper($jsonObj->class) == "USER"){            
            
            if (($jsonObj->email || $jsonObj->username) && $jsonObj->password){                      

                $queryObjects = $this->apitomicDB->GetByProp("username", $jsonObj->username);
    
                if($queryObjects){
                    return false;                                
                } else {
                    $queryObjects = $this->apitomicDB->GetByProp("email", $jsonObj->email);
    
                    if($queryObjects){
                        foreach($queryObjects as &$obj) {
                            if($obj["class"] == "User"){                                
                                return false;  
                            }
                        }                              
                    }
                    
                    $queryObjects = $this->apitomicDB->GetByProp("coreUser", true, $this->utils->isStringtified('boolean'));    
                    if($queryObjects == false){
                        $jsonObj->coreUser = true;                               
                    }   

                    $jsonObj->password = md5($this->crypto->encrypt($jsonObj->password));
                    $jsonObj->idle = true;

                    if($this->apitomize($jsonObj)){                        

                        if($jsonObj->email && $this->config->useSMTP && $this->config->activation_after_reg){                        
                
                            $subject = $this->config->activation_subject;
        
                            $body = file_get_contents($this->config->activation_body_template);
                            
                            return $this->utils->SendEmail($jsonObj->email, $subject, $body, $this->config);
                        }

                        return true;
                    }                    
                }
            }
        }

        return false;
    }

    public function forgotpwdreq($jsonObj){

        if ($jsonObj->email){
            $queryObjects = $this->apitomicDB->GetByProp("email", $jsonObj->email);

            if($queryObjects != false){

                if($this->config->resetpwd_secure){//CREATES FPR AND SENDS SECURE LINK.

                    foreach($queryObjects as &$obj) {
                        if($obj["class"] == "ForgotPasswordRequest"){                            
                            //Removes existing FPR
                            $this->apitomicDB->Delete($obj["id"]);       
                            break;     
                        }
                    }
    
                    foreach($queryObjects as &$obj) {
                        if($obj["class"] == "User"){  
    
                            $newObj = new stdClass();
                            $newObj->class = "ForgotPasswordRequest";      
                            $newObj->id = $this->utils->generateID($this->apitomicDB);      
                            $newObj->email = $jsonObj->email;    
                            $newObj->userID = $obj["id"];
                            
                            if($this->apitomize($newObj)){
                                //SEND EMAIL;
                                if($this->config->useSMTP){

                                    $jwt = $this->getJWT(array(
                                        "fpr" => $newObj->id
                                    ));
    
                                    $subject = $this->config->resetpwd_subject;
                                
                                    $map = array( "secure_form_url" => $this->config->resetpwd_secure_form_url.'?token='.$jwt);
        
                                    $email_template = file_get_contents($this->config->resetpwd_secure_template);
        
                                    $body = $this->utils->replace_tags($email_template, $map);
                                    
                                    return $this->utils->SendEmail($jsonObj->email, $subject, $body, $this->config);
    
                                }

                                return "Failed sending email: None SMTP configuration found."; 
                            }
                
                            break;
                
                        }    
                    }
                } else {//SENDS PASSWORD
                    foreach($queryObjects as &$obj) {  
                        if($obj["class"] == "User"){

                            
                            if($this->config->useSMTP){  

                                //SEND PWD;
                                $newpassword = $this->utils->generatePassword();
            
                                $user = new stdClass();
                                $user->id = $obj["id"];
                                $user = $this->deapitomize($user);
                                $user->password = md5($this->crypto->encrypt($newpassword));
                                $user->provisionalPassword = true;
                                
                                if($this->apitomize($user)){
                
                                    $subject = $this->config->resetpwd_subject;
                                    
                                    $map = array( "new_password" => $newpassword);
                
                                    $body_template = file_get_contents($this->config->resetpwd_template);
                
                                    $body = $this->utils->replace_tags($body_template, $map);
                                    
                                    return $this->utils->SendEmail($jsonObj->email, $subject, $body, $this->config);            
                                }
        
                            } else {

                                return "Failed sending email: None SMTP configuration found.";

                            } 
                        }   
                    }
                }         
            }
        }

        return true;
    }

    public function resetpwd($jsonObj){

        try {
            
            // decode jwt
            $decoded = JWT::decode($jsonObj->token, "ForgotPasswordRequest", array('HS256'));   

            $fprQuery = $this->apitomicDB->GetByID($decoded->data->fpr);

            if($fprQuery != false){
                $forgotpwdreq = json_decode($fprQuery["jdoc"]);
            } else {
                //return "Sorry, looks like this request is not longer available."; 
                return false;
            }

            $obj = $this->apitomicDB->GetByID($forgotpwdreq->userID);
        
            // 1. Update User
                
            if($obj){  
                
                $row = new APItomicDataRow($obj["id"], $obj["class"], $obj["parent"]);
                $user = json_decode($obj["jdoc"]);
                
                $newPassword = $jsonObj->pwd;
                $confirmPassword = $jsonObj->cpwd;

                if($newPassword != NULL && $confirmPassword != NULL){

                    if( $newPassword === $confirmPassword ){
                
                        $user->password = md5($this->crypto->encrypt($newPassword));
                        $row->jdoc = json_encode($user);
            
                        if($this->apitomicDB->Update($row)){
                            // 2. Removes FPR
                            $this->apitomicDB->Delete($decoded->data->fpr);
                            
                            // 3. Sends User Alert
                            
                            //SEND EMAIL;
                            if($this->config->useSMTP){                
                                $subject = $this->config->resetpwd_secure_user_alert_subject;
            
                                $body = file_get_contents($this->config->resetpwd_secure_user_alert_template);
                                
                                return $this->utils->SendEmail($user->email, $subject, $body, $this->config); 
                            }                
                    
                            // 3. Show Confirmation Message
                            //$message = "Password changed successfully.";
                            return true;  
                        }  
                    }
                }        
            }    
            return false;      
        } catch(Exception $e){
                //return "Sorry, looks like this request is not longer available.";      
                return false; 
        }
    }

    private function UpdateHasDuplicates($obj){
        
        $queryObjects = $this->apitomicDB->GetByProp("username", $obj->username);

        if ($queryObjects){
            foreach ($queryObjects as $objectkey => $object) {
                foreach ($object as $key => $value) {
                    if ($key == "id" && $value == $obj->id){
                        unset($queryObjects[$objectkey]);
                    }
                }
            }

            if(count($queryObjects) > 0){
                return true;
            } else {
                $queryObjects = $this->apitomicDB->GetByProp("email", $obj->email);

                if ($queryObjects){
                    foreach ($queryObjects as $objectkey => $object) {
                        foreach ($object as $key => $value) {
                            if ($key == "id" && $value == $obj->id)
                                unset($queryObjects[$objectkey]);
                        }
                    }
        
                    if(count($queryObjects) > 0){
                        return true;
                    }        
                }
            }
        }

        return false;
    }

    public function updateUser($jwt, $jsonObj){
        $tokenValidation = $this->validateToken($jwt);
        if ($tokenValidation->message == "Access granted."){
            if(strtoupper($jsonObj->class) == "USER"){
                if (($jsonObj->email || $jsonObj->username) && $jsonObj->password){

                    $hasDuplicates = $this->UpdateHasDuplicates($jsonObj);

                    if(!$hasDuplicates){
                        $password = $jsonObj->password;
                        $newPassword = $jsonObj->newpassword;
                        $confirmPassword = $jsonObj->confirmpassword;

                        if($password != NULL && $newPassword != NULL && $confirmPassword != NULL){

                            if( md5($this->crypto->encrypt($password)) === $this->currentUser->password ){

                                if( $newPassword === $confirmPassword ){
    
                                    if (md5($this->crypto->encrypt($newPassword)) != $this->currentUser->password){                        
                                        $jsonObj->password = md5($this->crypto->encrypt($newPassword));                                        
                                        unset($jsonObj->newpassword);                                
                                        unset($jsonObj->confirmpassword);                     
                                    }

                                }    
                            }
                        }
                        
                        $updatedUser = (object) array_merge((array) $this->currentUser, (array) $jsonObj);                        
                        $updatedUser = $this->apitomize($updatedUser);

                        if($updatedUser){

                            $jwt = $this->getJWT(array(
                                "id" => $updatedUser->id
                            ));
                            
                            $newObj = new stdClass();
                            $newObj->user = $this->getUserPublicInfo($updatedUser);
                            $newObj->jwt = $jwt;

                            return $newObj;
                        }
                    }
                    //                        
                }
            }
        } else if ($tokenValidation->message == "Access denied."){
            return $tokenValidation;
        }

        return false;
    }

    public function auth($jsonObj){        

        if(strtoupper($jsonObj->class) == "USER"){

            if (($jsonObj->email || $jsonObj->username) && $jsonObj->password){

                $reaction = $this->getReaction("username", $jsonObj->username);

                if ($reaction == false){                    
                    $reaction = $this->getReaction("email", $jsonObj->email);
                }

                if ($reaction){
                    if ($reaction->count > 0){

                        foreach($reaction->members as &$obj) {

                            if (strtoupper($obj->class) == "USER"){
                                $user = $obj;
                                break;
                            }

                        }

                        if ($user->password == md5($jsonObj->password)){                            

                            $jwt = $this->getJWT(array(
                                "id" => $user->id
                            ));                            
                            
                            $newObj = new stdClass();
                            $newObj->user = $this->getUserPublicInfo($user);
                            $newObj->jwt = $jwt;

                            return $newObj;
                        }                            
                    }
                }
            }
        }

        return false;
    }

    private function getUserPublicInfo($user){

        unset($user->id);
        unset($user->parent);
        unset($user->password);
        unset($user->coreUser);
        unset($user->idle);
        unset($user->provisionalPassword);

        return $user;
    }

    private function getJWT($data){

        date_default_timezone_set('UTC');

        $token = array(
            "iss" => $this->config->iss,
            "aud" => $this->config->aud,
            "iat" => time(),
            "nbf" => time(),
            "exp" => time() + ((int)$this->config->exp)*60,
            "data" => $data
        );
        
        // generate jwt
        $jwt = JWT::encode($token, $this->config->key);

        if ($jwt)
            return $this->crypto->encrypt($jwt);
        else
            return false;

    }

    public function invalidateToken($jwt){
        
        if($jwt){
            $tokenValidation = $this->validateToken($jwt);
            if ($tokenValidation->message == "Access granted."){

                $newObj = new stdClass();
                $newObj->class = "InvalidToken";
                $newObj->jwt = $jwt;
                $newObj->timestamp = time();
                
                $newObj = $this->apitomize($newObj);

                return $newObj;

            }
        }

        return false;

    }

    public function validateToken($jwt){
        
        // if decode succeed, show user details
        try {            
                    
            $invalidTokens = $this->apitomicDB->GetByProp("class", "InvalidToken");

            if ($invalidTokens){

                foreach($invalidTokens as &$obj) {                      
                    $invalidToken = new stdClass();
                    $invalidToken->id = $obj["id"];
                    $invalidToken = $this->deapitomize($invalidToken);

                    $timestamp = (int)$invalidToken->timestamp;
                    $timeReachedBy = time() - $timestamp;

                    if( $timeReachedBy > ((int)$this->config->exp)*60){
                        
                        $this->apitomicDecay($invalidToken);

                    } else {

                        if($invalidToken->jwt == $jwt){      
    
                            $newObj = new stdClass();
                            $newObj->message = "Access denied.";
    
                            return $newObj;
                        }

                    }
    
                }
            } 

            // decode jwt
            $decoded = JWT::decode($this->crypto->decrypt($jwt), $this->config->key, array('HS256'));        

            $newObj = new stdClass();
            $newObj->id = $decoded->data->id;
            $this->currentUser = $this->deapitomize($newObj);

            $timeRemaining = $decoded->exp - time();

            if ($timeRemaining > 0 && $timeRemaining <= 60){                
                
                $newJwt = $this->getJWT($decoded->data);
                
            }
    
            $newObj = new stdClass();
            $newObj->message = "Access granted.";
            $newObj->data = $decoded->data;
            if ($newJwt)
                $newObj->jwt = $newJwt;

            return $newObj;
    
        }        
        // if decode fails, it means jwt is invalid
        catch (Exception $e){            
            // tell the user access denied  & show error message            
    
            $newObj = new stdClass();
            $newObj->message = "Access denied.";
            $newObj->data = $e->getMessage();

            return $newObj;
        }
    }
    
    public function secureObj($obj, $key=NULL){
        if (is_array($obj)){
            foreach($obj as &$object){
                if(isset($object->id)){
                    if($this->currentUser->coreUser != "true"){
                        $storedObj = clone $object;
                        $storedObj = $this->deapitomize($storedObj);
                        if ($storedObj->apitomizedby != $this->currentUser->id){                                
                            $object->error = "Unable to update.";         
                        }
                    }
                } else {
                    $object->apitomizedby = $this->currentUser->id;
                }
            }

        } else {
            if(isset($obj->id)){
                if(!$this->currentUser->coreUser){
                    $storedObj = clone $obj;
                    $storedObj = $this->deapitomize($storedObj);
                    if ($storedObj->apitomizedby != $this->currentUser->id){                                
                        $obj->error = "Unable to update.";          
                    }
                }
            } else if(is_object($obj)) {
                $obj->apitomizedby = $this->currentUser->id;
            }
        }

        if(is_array($obj) || is_object($obj))
            array_walk_recursive($obj, 'APItomic::secureObj');
    }

    public function getReaction($key, $value=NULL, $filter=NULL, $decay=false){
                
        $objectsArray = [];

        if ($key == "chainreaction"){   
            $objectsArray = $this->apitomicChainReaction(strpos($value, 'owned') !== false);
            if($objectsArray == false){       
                return false;
            }
        } else { 
            $queryObjects = $this->apitomicDB->GetByProp($key, $value, $this->utils->isStringtified($filter));
    
            if($queryObjects == false){
    
                if ($this->apitomicDB->objExists($key)){
                    
                    $newObj = new stdClass();
                    $newObj->id = $key;
                    $newObj = $this->deapitomize($newObj);                                                        

                    if($newObj->class == "APITomicStoredFormula" && $decay == false){

                        $newObj = $this->formula->getResult($newObj);

                    } 

                    if($this->config->useJWT){

                        if ($this->currentUser->coreUser){
                            if (strpos($filter, 'owned') !== false){

                                if ($newObj->apitomizedby == $this->currentUser->id)

                                    return $newObj;

                            } else {                      
                                return $newObj;
                            }                
                        } else if($newObj->apitomizedby == $this->currentUser->id || $newObj->apitomizedby == "public"){
                            return $newObj;
                        } else {
                            return false;
                        }

                    } else {                        
                        return $newObj;
                    }  
                }
                else {
                    return false;
                }
    
            } else {

                foreach($queryObjects as &$obj) { 

                    $newObj = new stdClass();
                    $newObj->id = $obj["id"];
                    $newObj = $this->deapitomize($newObj);
                    
                    if($this->config->useJWT){

                        if ($this->currentUser->coreUser){
                            if (strpos($filter, 'owned') !== false){

                                if ($newObj->apitomizedby == $this->currentUser->id)
                                    array_push($objectsArray, $newObj);

                            } else {                                         
                                array_push($objectsArray, $newObj);
                            }
                        } else if($newObj->apitomizedby == $this->currentUser->id || $newObj->apitomizedby == "public"){
                            array_push($objectsArray, $newObj);
                        }

                    } else {                            
                        array_push($objectsArray, $newObj);
                    } 
    
                }
            }            
        }
        
        $newObj = new stdClass();
        $newObj->key = $key;
        $newObj->count = count($objectsArray);
        $newObj->members = $objectsArray;

        return $newObj;
    }

    private function objectMapper($prop, $key=NULL, $parent=NULL)
    {    
        if(is_object($prop)) {
            
            if ($prop->coreUser){

                $queryObjects = $this->apitomicDB->GetByProp("coreUser", true, $this->utils->isStringtified('boolean'));
    
                if($queryObjects !== false){
                    return false;                                
                }               
            }
            
            if (!$prop->class)
                $prop->class = "any";

            if (!$prop->id)
                $prop->id = $this->utils->generateID($this->apitomicDB);
                
            if ($parent)
                $prop->parent = $parent;
            
            $row = new APItomicDataRow($prop->id, $prop->class, $prop->parent);  

            array_walk_recursive($prop, 'APItomic::objectMapper', $prop->id);
            
            unset($prop->class);
            unset($prop->id);
            unset($prop->parent);

            $row->jdoc = json_encode($prop);
            //FLAG UPDATE!!!
            if ($this->apitomicDB->objExists($row->id)){
                $storedObj = $this->apitomicDB->GetByID($row->id);
                if ($storedObj["class"] == $row->class){
                           
                    $this->apitomicDB->Update($row);

                }else{       
                    return false;
                }
            } else {         
                $this->apitomicDB->Add($row); 
            }          
            
            $prop->id = $row->id;    
            foreach($prop as $key => $value)  {  
                if ($key!="id")
                    unset($prop->$key);          
            } 
        }
    }

    private function getChilds($prop, $key){  
        if ($prop->id){
            $childs = $this->getReaction("parent", $prop->id);

            if($childs){
                foreach ($childs->members as $member) {
                
                    array_walk_recursive($member, 'APItomic::getChilds');                
                    
                    if (!in_array($member->id, $this->childsArr))
                    {
                        $member->parent = NULL;
                        $this->apitomicDecay($member);
                    }
                }
            }
        }
    }

    private function getUpdatedChilds($prop, $key){ 

        if ($prop->id){   

            foreach ($prop as $k => $val){                
            
                if (is_object($val)){ 
                    //echo "id: ".$val->id."<br>";                   
                    array_push($this->childsArr, $val->id);
                    array_walk_recursive($val, 'APItomic::getUpdatedChilds');
                } else if (is_array($val)){
                    foreach ($val as $v)
                    {
                        if (is_object($v)){
                            //echo "id: ".$v->id."<br>";     
                            array_push($this->childsArr, $v->id);
                            array_walk_recursive($v, 'APItomic::getUpdatedChilds');
                        }
                    }
                }
            }
        }
    }

    private function deleteMissedChilds($obj){
                         
        $this->childsArr = [];
        
        if ($obj->id){  
            foreach ($obj as $k => $val){
                if (is_object($val)){  
                    //echo "id: ".$val->id."<br>";                           
                    array_push($this->childsArr, $val->id);
                } else if (is_array($val)){
                    foreach ($val as $v)
                    {
                        if (is_object($v)){ 
                            //echo "id: ".$v->id."<br>";     
                            array_push($this->childsArr, $v->id);
                        }
                    }
                }
            }
            
            array_walk_recursive($obj, 'APItomic::getUpdatedChilds');

            // var_dump($this->childsArr);
            
            array_walk_recursive($obj, 'APItomic::getChilds');

            $childs = $this->getReaction("parent", $obj->id);
            if($childs){
                foreach ($childs->members as $member) {
                    if (!in_array($member->id, $this->childsArr))
                    {
                        $member->parent = NULL;
                        $this->apitomicDecay($member);
                    }
                }  
            }             
        }
    }

    public function apitomize($jsonObj, $removeChilds = true){//ADD

        if(gettype($jsonObj) == 'array'){
            $objectsArray = [];
            foreach($jsonObj as &$obj)  {
                         
                if ($removeChilds)
                    $this->deleteMissedChilds($obj);

                if($this->objectMapper($obj) === false){
                    return false;
                }else{
                    array_push($objectsArray, $this->deapitomize($obj)); 
                }
            } 

            return $objectsArray;
        } else {

            if($jsonObj->class == "APITomicFormula"){  

                $result = $this->formula->getResult($jsonObj);

                return $result;

            } else {
                         
                if ($removeChilds)
                    $this->deleteMissedChilds($jsonObj);                          
    
                if($this->objectMapper($jsonObj) === false){
                    return false; 
                }
                    
                return $this->deapitomize($jsonObj);

            }
        } 

        return false;
    }

    private function objectBuilder($prop, $key)
    {    
        if(is_object($prop)) {  

            if ($prop->id){
                
                $row = $this->apitomicDB->GetByID($prop->id);

                if ($row){
                    $prop->class = $row["class"];
                    $prop->parent = $row["parent"];
                    $newprop = json_decode($row["jdoc"]);                     
                    foreach($newprop as $key => $value)  {  
                        $prop->$key = $value;   
                    } 
                }

            }

            array_walk_recursive($prop, 'APItomic::objectBuilder');
        }
    }

    public function deapitomize ($jsonObj){//GET

        if ($jsonObj){
            if(gettype($jsonObj) == 'array'){ 
                if (count($jsonObj) > 0){
                    foreach($jsonObj as &$obj)  {           
                    
                        if ($obj->id){
                            $row = $this->apitomicDB->GetByID($obj->id);
                            if ($row){
                                $obj->class = $row["class"];
                                $obj->parent = $row["parent"];
                                $newprop = json_decode($row["jdoc"]);                     
                                foreach($newprop as $key => $value)  {  
                                    $obj->$key = $value;   
                                } 
            
                                if(array_walk_recursive($obj, 'APItomic::objectBuilder'))                            
                                    return $jsonObj;
                            } 
                        }      
                    }

                }
            } else {    
                if ($jsonObj->id){       
                    $row = $this->apitomicDB->GetByID($jsonObj->id);
                    if ($row){
                        $jsonObj->class = $row["class"];
                        $jsonObj->parent = $row["parent"];
                        $newprop = json_decode($row["jdoc"]);                     
                        foreach($newprop as $key => $value)  {  
                            $jsonObj->$key = $value;   
                        }                   
        
                        if(array_walk_recursive($jsonObj, 'APItomic::objectBuilder'))                    
                            return $jsonObj;
                    }
                } 
            } 

        }

        return false;
    }

    private function objectRemover($prop, $key, $childID)
    {   
        if (is_object($prop) || is_array($prop)) {

            foreach($prop as $key => $value)  {         
                if (is_object($value)) {
                    if ($value->id == $childID) {
                        $prop->$key = NULL;
                        break;
                    }    
                } else if (is_array($value)) {
                    foreach($value as $key => $val)  {
                        if ($val->id == $childID) {                        
                            $arr = $prop->$key;
                            unset($arr[$k]);
                            $prop->$key = array_values($arr);
                            break;
                        }  
                    }
                }
            }
                
            array_walk_recursive($prop, 'APItomic::objectRemover', $childID); 
        }
    }

    private function objectDestroyer($prop, $key)
    {    
        if(is_object($prop)) {  

            if ($prop->id){
                
                if($this->config->useJWT){
                    if($prop->apitomizedby != "public"){
                
                        $this->apitomicDB->Delete($prop->id); 
                        
                    }
                } else {               
                
                    $this->apitomicDB->Delete($prop->id); 

                }
            }

            array_walk_recursive($prop, 'APItomic::objectDestroyer');

        }
    }

    private function removeFromParent($parent, $childID){

        //Remuevo elemento del padre    
        foreach($parent as $key => $value)  {         
            if (is_object($value)) {
                if ($value->id == $childID) {
                    $parent->$key = NULL;
                    break;
                }    
            } else if (is_array($value)) {
                foreach($value as $k => $val)  {
                    if ($val->id == $childID) {
                        $arr = $parent->$key;
                        unset($arr[$k]);
                        $parent->$key = array_values($arr);
                        break;
                    }  
                }
            }
        }

        array_walk_recursive($parent, 'APItomic::objectRemover', $childID); 

        $this->apitomize($parent, false);
    }

    public function apitomicDecay($jsonObj){//DELETE

        //1 - Removes from Parent
        if ($jsonObj->parent){

            $newObj = new stdClass();
            $newObj->id = $jsonObj->parent;
            $parent = $this->deapitomize($newObj);
        
            if($this->config->useJWT){
                if($jsonObj->apitomizedby != "public"){
        
                    $this->removeFromParent($parent, $jsonObj->id); 
                    
                }
            } else {               

                $this->removeFromParent($parent, $jsonObj->id); 

            }
        } else {
            if(is_array($jsonObj)){
                foreach($jsonObj as $k => $val)  {
                    if ($val->parent){
                        $newObj = new stdClass();
                        $newObj->id = $val->parent;
                        $parent = $this->deapitomize($newObj);           
        
                        if($this->config->useJWT){
                            if($val->apitomizedby != "public"){
                    
                                $this->removeFromParent($parent, $val->id);  
                                
                            }
                        } else {               

                            $this->removeFromParent($parent, $val->id);  
                            
                        }             

                    }
                }
            }
        }
           
        if (is_object($jsonObj) || is_array($jsonObj)){

            //2 - Removes Childs
            $result = array_walk_recursive($jsonObj, 'APItomic::objectDestroyer');

            if ($jsonObj->id){       

                //3 - Removes Object       
                if($this->config->useJWT){
                    if($jsonObj->apitomizedby != "public"){
            
                        return $this->apitomicDB->Delete($jsonObj->id);
                        
                    }
                } else {               

                    return $this->apitomicDB->Delete($jsonObj->id);

                }

            } else {

                return $result;

            }
        } 

        return false;    
    }
    
    public function getRelativityMask(){//GETS MASK INFO

        if($this->config->namespace){
            $maskKeys = array(                
                "apitomize",
                "decay",
                "registeruser",
                "auth",
                "updateuser",
                "forgotpwdreq",
                "resetpwd",
                "invalidatetoken"
            );

            if($this->config->mask){
                foreach($this->config->mask as $key => $value) {
                    if (isset($value) && trim($value)!=="" && $value!==$key){                          
                        $i = array_search(trim(strtolower($key)), $maskKeys); 
                        if($i !== -1){   
                            $maskKeys[$i] = trim(strtolower($value));
                        }
                    }
                }
            }

            date_default_timezone_set('UTC');

            $token = array(
                "data" => $maskKeys
            );
            
            // generate jwt
            $newJwt = JWT::encode($token, strval(time()));

            return $newJwt;
        }

        return false;
    }
    
    public function bigBang(){//DELETE ALL DATA

        if($this->config->useJWT){

            if ($this->currentUser->coreUser){            
                return $this->apitomicDB->ClearAll();
            }

        } else {
            return $this->apitomicDB->ClearAll();
        }

        return false;
    }

    public function apitomicChainReaction($owned = false){//BKP
        //TODO!
        $parents = $this->apitomicDB->GetAllParents();
        $objectsArray = [];

        if($parents){
            foreach($parents as &$parent) {  

                $newObj = new stdClass();
                $newObj->id = $parent["id"];
                $newObj = $this->deapitomize($newObj);
                
                if($this->config->useJWT){

                    if ($this->currentUser->coreUser && !$owned){                                      
                        array_push($objectsArray, $newObj);
                    } else if($this->currentUser->coreUser && $owned && $newObj->apitomizedby == $this->currentUser->id){
                        array_push($objectsArray, $newObj);
                    } else if($newObj->apitomizedby == $this->currentUser->id || $newObj->apitomizedby == "public"){
                        array_push($objectsArray, $newObj);
                    }

                } else {                            
                    array_push($objectsArray, $newObj);
                } 

            }

            return $objectsArray;
        }

        return $parents;

    }
}

?>