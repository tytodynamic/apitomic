﻿import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { APItomicService } from '@tytodynamic/apitomic-angular/lib'
import { Car } from '../_models';

@Component({selector: 'carpage', templateUrl: 'carpage.component.html'})
export class CarPageComponent implements OnInit {
    car:Car = {
        brand: '',
        model: '',
        year: '',
        specs: '',
        price: '0.00',
        imageURL: '',                     
        ...(new Car).spread()
    };
    carForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    @Output() onCarUpdate = new EventEmitter();
    @Input() title:string;

    constructor(
        private formBuilder: FormBuilder,
        private apitomicService: APItomicService) {}

    ngOnInit() {
        this.loading = false;

        this.carForm = this.formBuilder.group({
            brand: ['', Validators.required],
            model: ['', Validators.required],
            year: [''],
            specs: [''],
            price: [''],
            imageURL: ['']
        });
    }

    updateCarForm(car: Car){
        this.car = car;
        this.carForm.setValue({
            brand: this.car.brand,
            model: this.car.model,
            year: this.car.year,
            specs: this.car.specs,
            price: this.car.price,
            imageURL: this.car.imageURL
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.carForm.controls; }

    onSubmit() {
        
        this.submitted = true;

        // stop here if form is invalid
        if (this.carForm.invalid) {
            return;
        }

        this.loading = true;

        this.car.brand = this.f.brand.value,
        this.car.model = this.f.model.value,
        this.car.year = this.f.year.value,
        this.car.specs = this.f.specs.value,
        this.car.price = this.f.price.value,
        this.car.imageURL = this.f.imageURL.value

        this.apitomicService.apitomize(this.car, sessionStorage.getItem("jwt"))
        .subscribe(
            data => {                  
                this.loading = false;               
                this.onCarUpdate.emit();                   
                document.getElementById('id04').style.display='none';
            },
            error => {
                this.error = error;
                this.loading = false;
            });
        
    }

    closeModal() {
        document.getElementById('id04').style.display='none';
    }
}
