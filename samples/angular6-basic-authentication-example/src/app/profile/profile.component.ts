﻿import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { APItomicService } from '@tytodynamic/apitomic-angular/lib'
import { User } from '../_models';

@Component({selector: 'profile', templateUrl: 'profile.component.html'})
export class ProfileComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';   
    currentUser: User;

    @Output() onUserUpdate = new EventEmitter();

    constructor(
        private formBuilder: FormBuilder,
        private apitomicService: APItomicService) {}

    ngOnInit() {
        this.loading = false;

        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            password2: ['']
        }, {validator: this.checkPasswords});

        this.currentUser = JSON.parse(sessionStorage.getItem('user'));

        this.loginForm.setValue({
            username: this.currentUser.username,
            password: this.currentUser.password,
            password2: ''
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        if (confirm("Are you sure to save this changes in your account?")){
            this.submitted = true;

            // stop here if form is invalid
            if (this.loginForm.invalid) {
                return;
            }

            if (this.f.password2.value != this.f.password.value){
                return;
            }

            this.loading = true;

            this.currentUser.username = this.f.username.value;
            this.currentUser.password = this.f.password.value;

            this.apitomicService.updateUser(this.currentUser, sessionStorage.getItem("jwt"))
            .subscribe(
                data => {                    
                    sessionStorage.setItem('user', JSON.stringify(data)); 
                    this.loading = false;               
                    this.onUserUpdate.emit();                   
                    document.getElementById('id03').style.display='none';
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
        }
    }

    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
        let pass = group.controls.password.value;
        let password2 = group.controls.password2.value;

        return pass === password2 ? null : { notSame: true }     
    }

    closeModal() {
        document.getElementById('id03').style.display='none';
    }
}
