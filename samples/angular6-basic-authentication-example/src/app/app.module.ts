﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { ErrorInterceptorProvider, APItomicServiceProvider, apiUrl, namespace, relativity } from '@tytodynamic/apitomic-angular/lib';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { ProfileComponent } from './profile';
import { CarPageComponent } from './carpage';
import { CarCardComponent } from './components';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        ProfileComponent,
        CarPageComponent,
        CarCardComponent
    ],
    providers: [
    // IF YOU WANT TO SET REACTOR WITHOUT WEBPACK CONFIG FILE
    // {provide: apiUrl, useValue: 'http://localhost:81'},
    // {provide: namespace, useValue: 'apitomic'},
    // {provide: relativity, useValue: 'relativity'},
      {provide: apiUrl, useValue: config.apiUrl},
      {provide: namespace, useValue: config.namespace},
      {provide: relativity, useValue: config.relativity},
      ErrorInterceptorProvider,
      APItomicServiceProvider
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }