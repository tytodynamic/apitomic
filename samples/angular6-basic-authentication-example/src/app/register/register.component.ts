﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { APItomicService } from '@tytodynamic/apitomic-angular/lib'

@Component({selector: 'register', templateUrl: 'register.component.html'})
export class RegisterComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private apitomicService: APItomicService) {}

    ngOnInit() {
        this.loading = false;
        
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            password2: ['']
        }, {validator: this.checkPasswords});

        // reset login status
        this.apitomicService.logout(sessionStorage.getItem('jwt'));

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        if (this.f.password2.value != this.f.password.value){
            return;
        }

        this.loading = true;

        var user = {
            class: "User",
            username: this.f.username.value,
            password: this.f.password.value
        }

        this.apitomicService.registerUser(user)
            .subscribe(
                data => {                   
                    this.loading = false; 
                    document.getElementById('id02').style.display='none';
                    this.router.navigate([this.returnUrl]);
                    //console.log(JSON.stringify(data));
                    alert(data.statusMessage);
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
    }

    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
        let pass = group.controls.password.value;
        let password2 = group.controls.password2.value;

        return pass === password2 ? null : { notSame: true }     
    }

    closeModal() {
        document.getElementById('id02').style.display='none';
    }
}
