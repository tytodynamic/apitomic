﻿import { Component, EventEmitter, Output, Input } from '@angular/core';
import { Car } from '../_models';

@Component({selector: 'carcard', templateUrl: 'carcard.component.html'})
export class CarCardComponent{

    @Output() onEdit = new EventEmitter();
    @Output() onDelete = new EventEmitter();
    @Input() car:Car;

    constructor() {}
}
