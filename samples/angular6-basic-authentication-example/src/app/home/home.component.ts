﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Car } from '../_models';
import { APItomicService } from '@tytodynamic/apitomic-angular/lib';
import { CarPageComponent } from '../carpage';

@Component({templateUrl: 'home.component.html'})
export class HomeComponent implements OnInit {
    cars: Car[] = [];    
    currentUser: any;  
    carPageTitle: string = 'Add Car';
    currentCar: Car = {
        brand: '',
        model: '',
        year: '',
        specs: '',
        price: '0.00',
        imageURL: '',                     
        ...(new Car).spread()
    };

    constructor (
        private apitomicService: APItomicService,
        private router: Router
    ) {}

    ngOnInit() {        
        this.currentUser = JSON.parse(sessionStorage.getItem('user'));
        this.cars = [];   

        this.apitomicService.reaction("class/car/owned", sessionStorage.getItem('jwt')).subscribe(reaction => { 
            this.cars = reaction;
        });
    }

    logoutClick(){
        

        this.apitomicService.logout(sessionStorage.getItem('jwt')).subscribe(data => {
            if(data.jwt == sessionStorage.getItem('jwt'))
                this.router.navigate(['/login']);
        });
        
    }

    profileClick(){
        document.getElementById('id03').style.display='block';
    }
    
    @ViewChild('carpage') carPage:CarPageComponent;
    addCarClick(){        
        this.carPageTitle = 'Add Car';
        this.currentCar = {
            brand: '',
            model: '',
            year: '',
            specs: '',
            price: '0.00',
            imageURL: '',                     
            ...(new Car).spread()
        }
        this.carPage.updateCarForm(this.currentCar);
        document.getElementById('id04').style.display='block';
    }
    editCarClick($event: { car: Car; }){
        this.carPageTitle = 'Edit Car';
        this.currentCar = $event.car;
        this.carPage.updateCarForm(this.currentCar);
        document.getElementById('id04').style.display='block';
    }

    deleteCarClick($event: { car: Car; }){
        if (confirm("Are you sure to delete this car?")){
            this.apitomicService.decay($event.car.id, sessionStorage.getItem('jwt')).subscribe(data => {                 
                console.log(data);  
                this.ngOnInit();      
                alert("Car Removed!"); 
            });
        }
    }
}