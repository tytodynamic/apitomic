import { APItomicObj } from "@tytodynamic/apitomic-angular/lib";

export class Car extends APItomicObj {
    brand: string;
    model: string;
    year: string;
    specs: string;
    price: string;
    imageURL: string;

    constructor () {
        super(Car.name);
    }
}