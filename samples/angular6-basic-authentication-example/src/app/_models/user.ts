﻿import { APItomicObj } from "@tytodynamic/apitomic-angular/lib";

export class User extends APItomicObj {
    username: string;
    password: string;

    constructor () {
        super(User.name);
    }
}