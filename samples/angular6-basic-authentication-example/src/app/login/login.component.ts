﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { APItomicService } from '@tytodynamic/apitomic-angular/lib'
import { User } from '../_models';

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private apitomicService: APItomicService) {}

    ngOnInit() {
        this.loading = false;
        
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // reset login status
        this.apitomicService.logout(sessionStorage.getItem('jwt'));

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;

        var user: User = {   
            username: this.f.username.value,
            password: this.f.password.value,                     
            ...(new User).spread()
        }

        this.apitomicService.auth(user)
            .subscribe(
                data => {                    
                    sessionStorage.setItem('user', JSON.stringify(data));
                    this.loading = false;
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
    }

    loginClick() {
        document.getElementById('id01').style.display='block';
    }

    registerClick() {
        document.getElementById('id02').style.display='block';
    } 

    closeModal() {
        document.getElementById('id01').style.display='none';
    }
}
