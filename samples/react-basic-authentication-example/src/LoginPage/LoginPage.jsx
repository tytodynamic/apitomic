import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { apitomicService } from '@tytodynamic/apitomic-react/lib';
import { RegisterPage } from '../RegisterPage';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        
        if (sessionStorage.getItem('jwt'))
            apitomicService.logout();

        this.state = {
            user: {
                class: 'User',
                id: '',
                username: '',
                password: ''
            },
            submitted: false,
            loading: false,
            error: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState(prevState => ({
            user: {
                ...prevState.user,
                [name]: value
            }
        }))
    }

    closeModal(e) {
        e.preventDefault();
        document.getElementById('id01').style.display='none';
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { user, returnUrl } = this.state;

        // stop here if form is invalid
        if (!(user.username && user.password)) {
            return;
        }

        this.setState({ loading: true });            

        // console.log('ENTRO: ' + JSON.stringify(this.state));

        apitomicService.auth(this.state.user)
            .then(
                user => {
                    sessionStorage.setItem('user', JSON.stringify(user));
                    const { from } = this.props.location.state || { from: { pathname: "/" } };
                    
                    document.getElementById('id01').style.display='none';
                    this.props.history.push(from);

                    // console.log("User: "+ JSON.stringify(user));
                },
                error => this.setState({ error, loading: false })
            );
    }  

    loginClick(e) {
        e.preventDefault();
        document.getElementById('id01').style.display='block';
    } 

    registerClick(e) {
        e.preventDefault();
        document.getElementById('id02').style.display='block';
    } 

    render() {
        const { user, submitted, loading, error } = this.state;
        return ( 
                         
            <div> 
                <button onClick={this.loginClick} style={{width: 'auto'}}>Login</button>
                <button onClick={this.registerClick} style={{width: 'auto'}}>Register</button>                                     
                <div id="id01" className="modal" ref="id01">   
                    <div className="modal-content animate">       
                        <div className="container" >
                            <span onClick={this.closeModal} className="close" title="Close Modal">&times;</span> 
                            <h3>Login</h3>  
                            <div className="imgcontainer">
                                <img src="src/image/016-user-icon.png" alt="Avatar" className="avatar"/>
                            </div>  
                            <form name="form" onSubmit={this.handleSubmit}>
                                <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
                                    <label htmlFor="username">E-Mail</label>
                                    <input type="text" className="form-control" name="username" value={user.username} onChange={this.handleChange} />
                                    {submitted && !user.username &&
                                        <div className="help-block">Username is required</div>
                                    }
                                </div>
                                <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                                    <label htmlFor="password">Password</label>
                                    <input type="password" className="form-control" name="password" value={user.password} onChange={this.handleChange} />
                                    {submitted && !user.password &&
                                        <div className="help-block">Password is required</div>
                                    }
                                </div>
                                <div className="form-group">
                                    <button className="btn btn-primary" disabled={loading}>Login</button>
                                    {loading &&
                                        <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                    }
                                </div>
                                {error &&
                                    <div className={'alert alert-danger'}>{error.statusMessage}</div>
                                }
                            </form>
                        </div>
                    </div>
                </div>                                         
                <Router>
                    <Route path="/login" component={RegisterPage} />
                </Router>         
            </div>
        );
    }
}

export { LoginPage }; 