import React from 'react';

class CarCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            car: {
                class: 'Car',
                brand: '',
                model: '',
                year: '',
                specs: '',
                price: '0.00',
                imageURL: ''
            }
        };
    }

    componentDidMount() {
        if(this.props.car)
            this.setState({ 
                car: this.props.car
            });
    }   

    render() {
        const {car} = this.state;
        return (                          
            <div className="col-sm-4">
                <div className="card mb-4">
                    <img className="card-img-top" data-src="holder.js/100px180/" alt="100%x180" src={(!car.imageURL || car.imageURL === '')?'src/image/jeep.png':car.imageURL} data-holder-rendered="true" style={{height: '180px', width: '100%', display: 'block'}}/>
                    <div className="card-body">
                        <h3 className="card-title">{car.brand + " " + car.model + " " + car.year}</h3>
                        <div className="scrollable-text"><p className="card-text">{car.specs}</p></div>
                    </div>
                    <div className="card-footer">
                        <strong className="text-muted">{"Price: $USD " + car.price}</strong>
                        <br />
                        <button className="button okbtn" onClick={this.props.onEdit} car={JSON.stringify(this.state.car)} >Edit</button>                        
                        <button className="button cancelbtn" onClick={this.props.onDelete} car={JSON.stringify(this.state.car)} >Delete</button>
                    </div>
                </div>
            </div>
        );
    }
}

export { CarCard }; 