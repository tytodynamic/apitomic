import React from 'react';

import { apitomicService } from '@tytodynamic/apitomic-react/lib';
import APItomicObj from '@tytodynamic/apitomic-react/lib/_helpers/apitomicObj';

class CarPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            car: {
                brand: '',
                model: '',
                year: '',
                specs: '',
                price: '0.00',
                imageURL: ''
            },
            submitted: false,
            loading: false,
            error: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    updateCarState(car){
        this.setState({ 
            car: car
        });
    }

    handleChange(e) {
        const { name, value } = e.target;

        this.setState(prevState => ({
            car: {
                ...prevState.car,
                [name]: value
            }
        }));
    }

    closeModal(e) {
        e.preventDefault();
        document.getElementById('id04').style.display='none';
    }

    handleSubmit(e) {
        e.preventDefault();

        //console.log(this.password.current.value + " | " + this.password2.current.value);

        this.setState({ submitted: true });
        const { car, returnUrl } = this.state;

        // stop here if form is invalid
        if (!(car.brand && car.model)) {
            return;
        }

        this.setState({ loading: true });            

        // console.log('ENTRO: ' + JSON.stringify(this.state));

        apitomicService.apitomize(new APItomicObj(this.state.car, 'Car'))
            .then(
                data => {                    
                    this.setState({ loading: false });
                    this.props.onCarUpdate();                   
                    document.getElementById('id04').style.display='none';
                },
                error => this.setState({ error, loading: false })
            );
    }  

    render() {
        const { car, submitted, loading, error } = this.state;
        return (                          
            <div id="id04" className="modal" ref="id04">   
                <div className="modal-content animate">       
                    <div className="container" >
                        <span onClick={this.closeModal} className="close" title="Close Modal">&times;</span> 
                        <h3>{this.props.title}</h3>
                        <div className="imgcontainer">
                            <img src={(!car.imageURL || car.imageURL === '')?'src/image/jeep.png':car.imageURL} alt="Avatar" className="avatar"/>
                        </div>      
                        <form name="form2" onSubmit={this.handleSubmit}>
                            <div className={'form-group' + (submitted && !car.brand ? ' has-error' : '')}>
                                <label htmlFor="brand">Brand</label>
                                <input type="text" className="form-control" name="brand" value={car.brand} onChange={this.handleChange} />
                                {submitted && !car.brand &&
                                    <div className="help-block">Car brand is required</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !car.model ? ' has-error' : '')}>
                                <label htmlFor="model">Model</label>
                                <input type="text" className="form-control" name="model" value={car.model} onChange={this.handleChange} />
                                {submitted && !car.model &&
                                    <div className="help-block">Car model is required</div>
                                }
                            </div>
                            <div className='form-group'>
                                <label htmlFor="year">Year</label>
                                <input type="text" className="form-control" name="year" value={car.year} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="specs">Specifications</label>
                                <textarea className="form-control" name="specs" value={car.specs} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="price">Price (USD$)</label>
                                <input type="text" className="form-control" name="price" value={car.price} onChange={this.handleChange} />
                            </div>
                            <div className='form-group'>
                                <label htmlFor="imageURL">Image URL</label>
                                <input type="text" className="form-control" name="imageURL" value={car.imageURL} onChange={this.handleChange} />
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary" disabled={loading}>Save</button>
                                {loading &&
                                    <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                }
                            </div>
                            {error &&
                                <div className={'alert alert-danger'}>{error.statusMessage}</div>
                            }
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export { CarPage }; 