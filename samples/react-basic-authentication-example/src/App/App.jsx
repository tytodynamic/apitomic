import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { PrivateRoute } from '../_components';
import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';

import { apitomicService } from '@tytodynamic/apitomic-react/lib';
import config from 'config';


class App extends React.Component {     

    // IF YOU WANT TO SET REACTOR WITHOUT WEBPACK CONFIG FILE
    // componentDidMount() {
    //     apitomicService.setReactor({
    //         apiUrl: 'http://localhost:81',
    //         namespace: 'apitomic',
    //         relativity: 'relativity'
    //     });
    // } 

    render() {

        return (
            <div className="container">
                <div>
                    <h2>APItomic & React - Authentication Example & Tutorial</h2>                        
                    <Router>
                        <div>
                            <PrivateRoute exact path="/" component={HomePage} />
                            <Route path="/login" component={LoginPage} />
                        </div>
                    </Router>
                </div>
            </div>
        );
    }
}

export { App }; 