import React from 'react';

import { apitomicService } from '@tytodynamic/apitomic-react/lib';
import { CarCard } from '../_components';
import { ProfilePage } from '../ProfilePage';
import { CarPage } from '../CarPage';

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            cars: [],
            carWindowTitle: "Add Car"
        };

        this.carPage = React.createRef();
    }

    componentDidMount() {
        this.setState({ 
            user: JSON.parse(sessionStorage.getItem('user')),
            cars: { loading: true }
        });
        apitomicService.reaction('class/car/owned').then(
            reaction => this.setState({ cars: reaction }),
            error => {                
                this.setState({
                    cars: { loading: false }
                });
            }
        );
    } 

    logoutClick(e) {
        e.preventDefault();
        apitomicService.logout().then(
            data => {

                if (data.jwt == sessionStorage.getItem('jwt'))
                    this.props.history.push({ pathname: "/login" });

            }
        );
    }

    profileClick(e) {
        e.preventDefault();
        document.getElementById('id03').style.display='block';
    } 

    addCarClick(e) {
        e.preventDefault();
        this.setState({
            carWindowTitle: "Add Car"
        });
        
        this.carPage.current.updateCarState({
            brand: '',
            model: '',
            year: '',
            specs: '',
            price: '0.00',
            imageURL: ''
        });

        document.getElementById('id04').style.display='block';
    } 

    editCarClick(e) {
        e.preventDefault();
        this.setState({
            carWindowTitle: "Edit Car"
        });

        this.carPage.current.updateCarState(JSON.parse(e.target.getAttribute('car')));

        document.getElementById('id04').style.display='block';
    }

    deleteCarClick(e) {
        e.preventDefault();

        if (confirm("Are you sure to delete this car?")){
            
            apitomicService.decay(JSON.parse(e.target.getAttribute('car')).id).then(
                data => {
                    console.log(data);  
                    this.componentDidMount();      
                    alert("Car Removed!");            
                },
                error => {
                    if (error.statusCode === 404)
                        this.props.history.push({ pathname: "/login" });
                }
            );
        }
    }

    render() {
        const { user, cars } = this.state;
        return (
            <div>
                <button onClick={this.profileClick} style={{width: 'auto'}}>My Account</button>
                <button onClick={this.logoutClick.bind(this)} style={{width: 'auto'}}>Logout</button>
                <br/>
                <br/>
                <h4>Hi {user.username}!</h4>
                <p>You're logged in using APItomic Authentication Feature!</p>
                                                                 
                <ProfilePage onUserUpdate = {this.componentDidMount.bind(this)} />  
                <CarPage title = {this.state.carWindowTitle} onCarUpdate = {this.componentDidMount.bind(this)} ref={this.carPage} />  

                <h3>Your Favorite Car List:</h3>
                <button onClick={this.addCarClick.bind(this)} style={{width: 'auto'}}><span style={{fontSize: "1.5rem"}}>+</span></button>
                <br/>
                {cars.loading && <em>Loading cars...</em>}
                {(cars.loading == false || cars.length === 0) && <em>You don't have any favorite yet!</em>}
                {(cars.length > 0) &&  
                    <div className="row">
                        {cars.map((car, index) =>
                            <CarCard car={car} onEdit={this.editCarClick.bind(this)} onDelete={this.deleteCarClick.bind(this)} key = {car.id}/> 
                        )}
                    </div>
                }                    
            </div>
        );
    }
}

export { HomePage };