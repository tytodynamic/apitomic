function APItomic(host) {
    this.host = host.replace(/\/?$/, '/');
    this.headers = { "Content-Type": "application/json; charset=utf-8" };
}

//AUTHENTICATION REQUESTS
APItomic.prototype.auth = function(user, callback, errorHandler) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": this.host+"apitomic/auth",
        "method": "POST",
        "headers": this.headers,
        "processData": false,        
        "dataType" : "text",
        "data": JSON.stringify(user)
    }
    
    $.ajax(settings).then(function (response) {
        callback(response);
    }).fail(function (error) {
        errorHandler(error.responseText);
    });
}

APItomic.prototype.registerUser = function(user, callback, errorHandler) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": this.host+"apitomic/registerUser",
        "method": "POST",
        "headers": this.headers,
        "processData": false,       
        "dataType" : "text",
        "data": JSON.stringify(user)
    }
    
    $.ajax(settings).then(function (response) {
        callback(response);
    }).fail(function (error) {
        errorHandler(error.responseText);
    });
}

APItomic.prototype.resetpasswordreq = function(account, callback, errorHandler) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": this.host+"apitomic/resetpwdreq",
        "method": "POST",
        "headers": this.headers,
        "processData": false,        
        "dataType" : "text",
        "data": JSON.stringify(account)
    }
    
    $.ajax(settings).then(function (response) {
        callback(response);
    }).fail(function (error) {
        errorHandler(error.responseText);
    });
}

APItomic.prototype.resetpassword = function(rpform, callback, errorHandler) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": this.host+"apitomic/resetpwd",
        "method": "POST",
        "headers": this.headers,
        "processData": false,        
        "dataType" : "text",
        "data": JSON.stringify(rpform)
    }
    
    $.ajax(settings).then(function (response) {
        callback(response);
    }).fail(function (error) {
        errorHandler(error.responseText);
    });
}

APItomic.prototype.updateUser = function(user, token, callback, errorHandler) {

    var headers =   { 
                        "Content-Type": "application/json; charset=utf-8" ,
                        "Authorization": "Bearer " + token
                    }

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": this.host+"apitomic/updateUser",
        "method": "POST",
        "headers": headers,
        "processData": false,
        "dataType" : "text",
        "data": JSON.stringify(user)       
    }
    
    $.ajax(settings).then(function (response) {
        callback(response);
    }).fail(function (error) {
        errorHandler(error.responseText);
    });
}

APItomic.prototype.invalidateToken = function(token, callback, errorHandler) {

    var headers =   { 
                        "Content-Type": "application/json; charset=utf-8" ,
                        "Authorization": "Bearer " + token
                    }

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": this.host+"apitomic/invalidateToken",
        "method": "POST",
        "headers": headers,
        "processData": false,
        "dataType" : "text",
        "data": ""
    }
    
    $.ajax(settings).then(function (response) {
        callback(response);
    }).fail(function (error) {
        errorHandler(error.responseText);
    });
}

//END - AUTHENTICATION REQUESTS

//POST
APItomic.prototype.apitomize = function(obj, callback, errorHandler, token = null) {

    var headers = this.headers;

    if (token){
        headers =   { 
                        "Content-Type": "application/json; charset=utf-8" ,
                        "Authorization": "Bearer " + token
                    }
    }

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": this.host+"apitomic/updateUser",
        "method": "POST",
        "headers": headers,
        "processData": false,
        "dataType" : "text",
        "data": JSON.stringify(obj)
    }
    
    $.ajax(settings).then(function (response) {
        callback(response);
    }).fail(function (error) {
        errorHandler(error.responseText);
    });
}

//GET
APItomic.prototype.reaction = function(query, callback, errorHandler, token = null) {

    var headers = this.headers;

    if (token){
        headers =   { 
                        "Content-Type": "application/json; charset=utf-8" ,
                        "Authorization": "Bearer " + token
                    }
    }

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": this.host+"apitomic/"+query,
        "method": "GET",
        "headers": headers,
        "processData": false,
        "dataType" : "text",
        "data": ""
    }
    
    $.ajax(settings).then(function (response) {
        callback(response);
    }).fail(function (error) {
        errorHandler(error.responseText);
    });
}

//DELETE
APItomic.prototype.decay = function(query, callback, errorHandler, token = null) {

    var headers = this.headers;

    if (token){
        headers =   { 
                        "Content-Type": "application/json; charset=utf-8" ,
                        "Authorization": "Bearer " + token
                    }
    }

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": this.host+"apitomic/"+query,
        "method": "DELETE",
        "headers": headers,
        "processData": false,
        "dataType" : "text",
        "data": ""
    }
    
    $.ajax(settings).then(function (response) {
        callback(response);
    }).fail(function (error) {
        errorHandler(error.responseText);
    });
}