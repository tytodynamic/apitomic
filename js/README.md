# APItomic

Es un plugin de php que te permite establecer un servicio API REST para guardar, manipular, y verificar objetos en un esquema de base de datos NoSQL. Expone una librería de javascript completamente integrable con los principales frameworks de javascript y ES6 que hay en el mercado como lo son React.js y Angular ya que utiliza mecanismos de async con ajax, promises y observables según sea la necesidad de integración.

For more info please visit http://apitomic.bitbucket.io