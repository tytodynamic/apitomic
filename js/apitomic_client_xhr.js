function APItomic(host) {
    this.host = host.replace(/\/?$/, '/');
}

//AUTHENTICATION REQUESTS
APItomic.prototype.auth = function(user, callback, errorHandler) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.onreadystatechange = function() { 
        if (xhr.readyState === 4){
            if (xhr.status === 200 || xhr.status === 201){                
                callback(xhr.responseText);
            } else {
                errorHandler(xhr.responseText);
            }
        }
    }
    xhr.open("POST", this.host+"apitomic/auth", true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(JSON.stringify(user));
}

APItomic.prototype.registerUser = function(user, callback, errorHandler) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.onreadystatechange = function() { 
        if (xhr.readyState === 4){
            if (xhr.status === 200 || xhr.status === 201){                
                callback(xhr.responseText);
            } else {
                errorHandler(xhr.responseText);
            }
        }
    }
    xhr.open("POST", this.host+"apitomic/registerUser", true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(JSON.stringify(user));
}

APItomic.prototype.resetpasswordreq = function(account, callback, errorHandler) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.onreadystatechange = function() { 
        if (xhr.readyState === 4){
            if (xhr.status === 200 || xhr.status === 201){                
                callback(xhr.responseText);
            } else {
                errorHandler(xhr.responseText);
            }
        }
    }
    xhr.open("POST", this.host+"apitomic/resetpwdreq", true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(JSON.stringify(account));
}

APItomic.prototype.resetpassword = function(rpform, callback, errorHandler, errorHandler) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.onreadystatechange = function() { 
        if (xhr.readyState === 4){
            if (xhr.status === 200 || xhr.status === 201){                
                callback(xhr.responseText);
            } else {
                errorHandler(xhr.responseText);
            }
        }
    }
    xhr.open("POST", this.host+"apitomic/resetpwd", true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(JSON.stringify(rpform));
}

APItomic.prototype.updateUser = function(user, token, callback, errorHandler) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.onreadystatechange = function() { 
        if (xhr.readyState === 4){
            if (xhr.status === 200 || xhr.status === 201){                
                callback(xhr.responseText);
            } else {
                errorHandler(xhr.responseText);
            }
        }
    }
    xhr.open("POST", this.host+"apitomic/updateUser", true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(JSON.stringify(user));
}

APItomic.prototype.invalidateToken = function(token, callback, errorHandler) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.onreadystatechange = function() { 
        if (xhr.readyState === 4){
            if (xhr.status === 200 || xhr.status === 201){                
                callback(xhr.responseText);
            } else {
                errorHandler(xhr.responseText);
            }
        }
    }
    xhr.open("POST", this.host+"apitomic/invalidateToken", true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(null);
}

//END - AUTHENTICATION REQUESTS

//POST
APItomic.prototype.apitomize = function(obj, callback, errorHandler, token = null) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.onreadystatechange = function() { 
        if (xhr.readyState === 4){
            if (xhr.status === 200 || xhr.status === 201){                
                callback(xhr.responseText);
            } else {
                errorHandler(xhr.responseText);
            }
        }
    }
    xhr.open("POST", this.host+"apitomic/apitomize", true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    if (token)
        xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(JSON.stringify(obj));
}

//GET
APItomic.prototype.reaction = function(query, callback, errorHandler, token = null) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.onreadystatechange = function() { 
        if (xhr.readyState === 4){
            if (xhr.status === 200 || xhr.status === 201){                
                callback(xhr.responseText);
            } else {
                errorHandler(xhr.responseText);
            }
        }
    }
    xhr.open("GET", this.host+"apitomic/"+query, true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    if (token)
        xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(null);
}

//DELETE
APItomic.prototype.decay = function(query, callback, errorHandler, token = null) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.onreadystatechange = function() { 
        if (xhr.readyState === 4){
            if (xhr.status === 200 || xhr.status === 201){                
                callback(xhr.responseText);
            } else {
                errorHandler(xhr.responseText);
            }
        }
    }
    xhr.open("DELETE", this.host+"apitomic/"+query, true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    if (token)
        xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(null);
}