<?php    
    // header("Content-Type: text/html; charset=UTF-8");   
         
    $config = readConfig();
    if($config->showReactor === false){   
        print_json(404, "Not Found", null);
        die();
    }

    function readConfig(){
        $defaultValues = '{
            "db_host": "localhost",    
            "db_port": "3306",
            "username": "root",
            "pwd": "root",
            "database": "tytodynamic",
            "tablename": "apitomic",
            "allowedOrigins": [
                "http://localhost:80",
                "http://localhost:81"
            ]
        }';
        $defaultConfigObj = json_decode($defaultValues);

        // // Get the contents of the JSON file 
        $strJsonFileContents = file_get_contents("../apitomic_dev.json");
        $apitomicConfigObj = json_decode($strJsonFileContents);

        $configObj = (object) array_merge((array) $defaultConfigObj, (array) $apitomicConfigObj);
        
        return $configObj;
    }
    
   
    // Esta funcion imprime las respuesta en estilo JSON y establece los estatus de la cebeceras HTTP
    function print_json($status, $mensaje, $data) {
        header("HTTP/1.1 $status $mensaje");
        header("Content-Type: application/json; charset=UTF-8");

        $response['statusCode'] = $status;
        $response['statusMessage'] = $mensaje;
        $response['data'] = $data;

        echo json_encode($response, JSON_PRETTY_PRINT);
    }
?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
          
    <!-- ===========================
    THEME INFO
    =========================== -->
    <meta name="description" content="A php and open source project that you can integrate to your website or simply to extend the functionality of your backend that allows you to establish a REST API service or BaaS midleware to save, manipulate, and verify objects under a NoSQL scheme, all this from your own server.">
    <meta name="keywords" content="php, open, source, project, integrate, website, backend, REST, API, service, BaaS, midleware, save, manipulate, verify, objects, NoSQL, scheme, own, server">
    <meta name="author" content="Tytodynamic">
    
    <!-- DEVEOPER'S NOTE:
    This is a free Bootstrap powered HTML template from EvenFly. Feel free to download, modify and use it for yourself or your clients as long there is no money involved.
    
    Please don't try to sale it from anywhere; because I want it to be free, forever. If you sale it, That's me who deserves the money, not you :)
    -->

    <!-- ===========================
    SITE TITLE
    =========================== -->
    <title>APItomic v1.0.1</title><!-- This is what you see on your browser tab-->
    
    <!-- ===========================
    FAVICONS
    =========================== -->
    <link rel="icon" href="img/favicon.png">
     
    <!-- ===========================
    STYLESHEETS
    =========================== -->    
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/preloader.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/animate.css">
        
    <!-- ===========================
    ICONS: 
    =========================== -->
    <link rel="stylesheet" href="css/simple-line-icons.css">    
    
    <!-- ===========================
    GOOGLE FONTS
    =========================== -->    
    <link href="https://fonts.googleapis.com/css?family=Bungee+Shade|Prompt|Cambo|Source+Code+Pro&display=swap" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  <!-- ===========================
   GOOGLE ANALYTICS (Optional)
   =========================== -->
    
    <!--Replace this line with your analytics code-->
     
    <!-- Analytics end-->
  
   </head>
    <body data-spy="scroll">
      <!-- Preloader -->
      <div id="preloader">           
          <div id="status">
              <div class="loadicon wow tada infinite" data-wow-duration="8s"><img src="img/loader.png"></div>
          </div>
      </div>

    <div id="mydialog" class="Modal is-hidden is-visuallyHidden">
        <!-- Modal content -->
        <div class="Modal-content">
            <label style="color: #cc3333">ERROR: CAN'T CONNECT TO DATA BASE!</label>
            <br />
            <br />
            <p style="color: #333333">

            Make sure that the <b>apitomic.json</b> was filled with the right info.<br />
            <br />
            <b>Note:</b> Remember, the <b>db_host</b> and <b>db_port</b> properties should be filled as follows: 
            <br />
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>"db_host": "&lt;yourhostname>",</b> 
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>"db_port": "&lt;port_number>"</b> 
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e.g. "db_host": "localhost",
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "db_port": "3306"
            <br />
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>WRONG WAY:</b> 
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"db_host": "http://localhost:3306"
            <br />
            </p>
            <br />                     
            <div class="footerlinks"><!-- FOOTER LINKS START -->            
                <ul>                    
                    <li><a href="https://apitomic.bitbucket.io/" target="_blank">>> LEARN MORE</a></li>               
                </ul>
            </div><!-- FOOTER LINKS END -->
        </div>
    </div>
	<!-- ===========================
    ABOUT SECTION START
    =========================== -->
     <div id="about" class="container">
        
        <!-- LEFT PART OF THE ABOUT SECTION -->
         <div class="apitomic-title">
            <div class="row">
             <!-- <h2 class="wow fadeInDown" data-wow-duration="2s">&Lambda;&pi;tomic</h2> -->
             <img src="img/apitomic_label.png" class="apitomic-logo" alt="APItomic">

			 <h4 class="wow fadeInUp" data-wow-duration="3s">v1.0.1</h4>
             </div> <!-- ABOUT INFO END -->
             
         </div><!-- LEFT PART OF THE ABOUT SECTION END -->
        <!--Left part end-->
         
         <!-- RIGHT PART OF THE ABOUT SECTION -->
         <div class="ant wow fadeInUp myphoto" data-wow-duration="4s">
             <img src="img/user.png" class="apitomic-ant" alt="Mamun Srizon">
         </div><!-- RIGHT PART OF THE ABOUT SECTION END -->        
     </div><!-- ABOUT SECTION END -->
       
    <!-- ===========================
    FOOTER START
    =========================== -->    
    <footer>
        <div class="container">             
            <div class="footerlinks"><!-- FOOTER LINKS START -->            
                <ul>
                    <li><span style="color: white;">STATUS: <span class="status-up">&#10004;</span></span></li>    
                    <li><a href="https://apitomic.bitbucket.io/" target="_blank">>> LEARN MORE</a></li>               
                </ul>
            </div><!-- FOOTER LINKS END -->
             
            <div class="copyright"><!-- FOOTER COPYRIGHT START -->
                <p><span class="copyright">
					© Copyright. All rights reserved. Tytodynamic 2019
				</span></p>
            </div><!-- FOOTER COPYRIGHT END -->
            <!-- FOOTER SOCIAL ICONS END -->
         </div>
     </footer><!-- FOOTER END -->
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.4.0.min.js"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    <!--Other necessary scripts-->
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/drifolio.js"></script>
    <script src="js/wow.min.js"></script>
    <script>new WOW().init();</script>
    <script>
        mydialog = document.getElementById("mydialog");
        function showModal(){
            mydialog.className = "Modal";
        }
    </script>
    <div> 
        <?php            
            $connected = false;
            $connected = geigerFound($config);

            if (!$connected){
                echo '<script language="javascript">';
                echo 'setTimeout(showModal, 2000);';
                echo 'document.getElementsByClassName("container")[0].classList.add("grayblur");';
                echo 'document.getElementsByClassName("container")[1].classList.add("grayblur");';
                echo 'document.getElementsByTagName("body")[0].classList.add("grayBodyBg");';
                echo '</script>';
            }

            function geigerFound($config)
            {
                $db_host = ($config->db_port != "")?$config->db_host.':'.$config->db_port:$config->db_host;
                $connection = mysqli_connect($db_host,$config->username,$config->pwd);
                if(!$connection)
                    return false;
        
                if(!mysqli_select_db($connection, $config->database)){
                    
                    DBLogout($connection);
                    return false;  
                }      
                
                DBLogout($connection);
                return true;
            }
                
            function DBLogout($connection)
            {
                if($connection){
                    return mysqli_close($connection);
                }
                return false;
            } 
        ?>
    </div>
  </body>
</html>