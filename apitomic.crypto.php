<?php

class APITomicCrypto
{
    private static $_instance;
    const CRYPTO_METHOD = "AES-256-CBC";
    
    public function __construct(){
        // constructor code here it will be called once only
        
        $CRYPTO_IV = self::getKey();
        $CRYPTO_KEY = self::getKey()."1907198131011994";
    }

    private function getKey(){
        // Get the contents of the JSON file 
        $strJsonFileContents = file_get_contents("./apitomic.json");
        $configObj = json_decode($strJsonFileContents);        
        return $configObj->key;
    }
        
    public function encrypt($input){
        $encrypted = base64_encode(openssl_encrypt ($input, self::CRYPTO_METHOD, $CRYPTO_KEY, OPENSSL_RAW_DATA, $CRYPTO_IV));
        return $encrypted;
    }
        
    public function decrypt($encrypted){
        $output = openssl_decrypt(base64_decode($encrypted), self::CRYPTO_METHOD, $CRYPTO_KEY, OPENSSL_RAW_DATA, $CRYPTO_IV);
        return $output;
    }
}

?>