1 - Make sure that the apitomic.json was filled with the right info.

    Note: Remember, the db_host and db_port properties should be filled as follows: 

        "db_host": "<yourhostname>", 
        "db_port": "<port_number>"
            
        e.g.    "db_host": "localhost", 
                "db_port": "3306"
    
        WRONG WAY: "db_host": "http://localhost:3306"

2 - If the .htaccess doesn't work you may need to change the httpd.conf or apache2.conf in one of the following locations:

        <YOUR_APP_SERVER_PATH>/conf/apache
        <YOUR_APP_SERVER_PATH>/apache/conf
        <YOUR_APP_SERVER_PATH>/etc/apache2
        <YOUR_APP_SERVER_PATH>/etc/httpd
        <YOUR_APP_SERVER_PATH>/etc/httpd/conf

    then find:

        <Directory  "<YOUR_PUBLIC_ROOT_PATH>"/>
            ...
            AllowOverride None
            ...
        </Directory>

    And then replace None with All.

        <Directory  "<YOUR_PUBLIC_ROOT_PATH>"/>
            ...
            AllowOverride All
            ...
        </Directory>

    And then restart the server.